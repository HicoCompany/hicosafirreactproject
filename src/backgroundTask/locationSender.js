import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { connect } from 'react-redux';
import BackgroundTimer from 'react-native-background-timer';
import Permissions from 'react-native-permissions';
import Repository from './../repository/Repository'
/*
Need this permission 
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
*/

class LocationSender extends Component {

    constructor(props) {
        super(props);
    }  
    
    async checkPermission() {
        var result = await Permissions.check('location');
        if (result != 'authorized' && result != 'restricted'){
            this.requestPermission();
        }
    }
    
    async requestPermission() {
        var result = await Permissions.request('location');   
        if (result != 'authorized' && result != 'restricted'){
            this.checkPermission();
        }
    }

    async sendMyCurrentLocationToServer(position) {
        if (position && position.coords) {
            const headers = null;
            const params = {
                UserName: this.props.userData.UserName,
                Password: this.props.userData.Password,
                LatitudeLocation: position.coords.latitude,
                LongitudeLocation: position.coords.longitude
            };
            try {
                const response = await Repository.PhoneAddLocationContractorApi(params,headers);
            } catch (error) {
            }
        } else {
            Alert.alert("No GPS Data !");
        }
    }

    async getCurrentLocation() {
        await navigator.geolocation.getCurrentPosition(
            position => {              
              this.sendMyCurrentLocationToServer(position); 
            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
          );
    }

    componentDidMount(){ 
        this.checkPermission();
        BackgroundTimer.runBackgroundTimer(() => { 
            if (this.props.isActive){
                this.getCurrentLocation();
            }
        }, 60000);          
    }

    render() {
        return (
            null
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userData: state.userData,
        isActive: state.userData.isActive
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(LocationSender);