import React from 'react';
// eslint-disable-next-line no-unused-vars
import { Text, TextPropTypes } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Colors from '@src/utility/Colors';

/**
 *
 * @param {{weight: 'normal' | 'bold' | 'light',color: 'placeholder' | 'text'} & TextPropTypes} param0
 */
export default function Label({
  style,
  weight = 'normal',
  color = 'text',
  ...props
}) {
  return <Text style={[styles[`${weight}${color}Label`], style]} {...props} />;
}

const defaults = {
  normalLabel: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
  },
  boldLabel: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  lightLabel: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '100',
    },
    '@media android': {
      fontFamily: '$IR_L',
    },
  },
  placeholder: {
    color: Colors.gray,
  },
  text: {
    color: Colors.textBlack,
  },
};
const styles = EStyleSheet.create({
  normaltextLabel: {
    ...defaults.normalLabel,
    ...defaults.text,
  },
  boldtextLabel: {
    ...defaults.boldLabel,
    ...defaults.text,
  },
  lighttextLabel: {
    ...defaults.lightLabel,
    ...defaults.text,
  },
  normalplaceholderLabel: {
    ...defaults.normalLabel,
    ...defaults.placeholder,
  },
  boldplaceholderLabel: {
    ...defaults.boldLabel,
    ...defaults.placeholder,
  },
  lightplaceholderLabel: {
    ...defaults.lightLabel,
    ...defaults.placeholder,
  },
});
