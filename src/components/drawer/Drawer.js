import React, { Component } from 'react';
import { TouchableOpacity, View, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import EStyleSheet from 'react-native-extended-stylesheet';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { exitApp } from '../../redux/actions';
import Utils from '../../utility/Utils';
import { hp, wp, normalize } from './../../responsive';
import Colors from './../../utility/Colors';

//ui
import Label from '@components/Label';

//assets
import LOGIN_LOGO from './../../assets/image/ic_logo_login.png';

class Drawer extends Component {
  constructor(props) {
    super(props);
    this.onLogOutClick = this.onLogOutClick.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return Utils.shallowCompare(this, nextProps, nextState);
  }

  onLogOutClick() {
    this.props.exitApp();
    this.props.navigation.replace('splash');
  }

  render() {
    return (
      <View style={drawerStyle.container}>
        <View style={drawerStyle.topLayer}>
          <FastImage
            source={LOGIN_LOGO}
            resizeMode={FastImage.resizeMode.contain}
            style={drawerStyle.logo}
          />
        </View>
        <ScrollView>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('profile')}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{'پروفایل کاربر'}</Label>
            <Icon
              name={'account-circle-outline'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ListPaymentFixedBoothScreen')
            }
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{'مدیریت پرداخت '}</Label>
            <Icon
              name={'credit-card-outline'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>
          {/* <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("ListRequestPackageScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Label style={drawerStyle.buttonText}>
                            {'تحویل جمع اوری پسماند'}
                        </Label>
                        <Icon
                            name={'md-basket'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('ListRequestFixedBoothScreen')
            }
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>
              {'تحویل جمع اوری پسماند غرفه ثابت'}
            </Label>
            <Icon name={'basket'} size={25} style={drawerStyle.buttonIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('RegisterScreen')}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{'ثبت همیار'}</Label>
            <Icon
              name={'trash-can-outline'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('WasteCollectionRequests')
            }
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>
              {'لیست درخواست های جمع‌ آوری همیار'}
            </Label>
            <Icon
              name={'format-list-bulleted'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>
          {/* <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("RequestListScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Label style={drawerStyle.buttonText}>
                            {'تحویل همیاری '}
                        </Label>
                        <Icon
                            name={'ios-list'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity> */}
          {/*  <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("ListHelperScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Label style={drawerStyle.buttonText}>
                            {'لیست همیارها'}
                        </Label>
                        <Icon
                            name={'md-basket'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                   */}

          {/* <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("ListHelperScreenDeliverWaste")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Label style={drawerStyle.buttonText}>
                            {'لیست همیارهای همیاری'}
                        </Label>
                        <Icon
                            name={'md-basket'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity> */}
          {/*
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("PhoneGetDeliverWasteForTransfer")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Label style={drawerStyle.buttonText}>
                            {'لیست تحویل همیاری(انتقال) '}
                        </Label>
                        <Icon
                            name={'ios-list'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                     */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('report')}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{'گزارشات'}</Label>
            <Icon name={'poll'} size={25} style={drawerStyle.buttonIcon} />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('notice')}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{'اطلاع رسانی'}</Label>
            <Icon name={'magnify'} size={25} style={drawerStyle.buttonIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('CashWithdrawalScreen')
            }
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{' کیف پول '}</Label>
            <Icon
              name={'credit-card-outline'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>
          {/*
                    <TouchableOpacity
                         onPress={() => this.props.navigation.navigate("IntroductionScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Label style={drawerStyle.buttonText}>
                            {'معرفی به دوستان'}
                        </Label>
                        <Icon
                            name={'md-gift'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                     */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('AboutScreen')}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>{'درباره ما'}</Label>
            <Icon
              name={'dots-horizontal'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onLogOutClick}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonText}>
              {'خروج از حساب کاربری'}
            </Label>
            <Icon
              name={'exit-to-app'}
              size={25}
              style={drawerStyle.buttonIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onLogOutClick}
            activeOpacity={0.7}
            style={drawerStyle.buttonItem}>
            <Label style={drawerStyle.buttonTextVersion}>
              {'ورژن : 0.0.15'}
            </Label>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const drawerStyle = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  topLayer: {
    height: hp('25%'),
    backgroundColor: Colors.statusBar,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: wp('30%'),
    width: wp('30%'),
  },

  buttonItem: {
    height: hp('7%'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginRight: 5,
  },
  buttonIcon: {
    marginLeft: 5,
    // width: 40,
    // height: 40,
    // backgroundColor: 'grey',
  },
  buttonText: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
    color: Colors.black,
    fontSize: normalize(13),
    textAlign: 'right',
  },
});

const mapStateToProps = state => {
  return {
    userData: state.userData || {},
  };
};

const mapActionToProps = {
  exitApp,
};

export default connect(mapStateToProps, mapActionToProps)(Drawer);
