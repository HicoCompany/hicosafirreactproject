import React from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';

//ui components
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Label from '@components/Label';
import { Appbar, TouchableRipple } from 'react-native-paper';

//utils
import { View } from 'native-base';

const HeaderBack = ({ headerTitle, onBackPress, backTitle }) => {
  return (
    <Appbar.Header>
      <TouchableRipple
        borderless
        onPress={onBackPress}
        style={styles.backButton}
        delayPressIn={0}
        rippleColor="white"
        delayPressOut={0}>
        <View style={styles.back}>
          <Icon name="chevron-left" color="white" size={28} />
          <Label style={styles.backTitle}>{backTitle || 'بازگشت'}</Label>
        </View>
      </TouchableRipple>
      <Label style={styles.title} weight="bold">
        {headerTitle}
      </Label>
    </Appbar.Header>
  );
};

const styles = StyleSheet.create({
  header: {},
  title: {
    color: '#ffffff',
    textAlign: 'right',
    flex: 1,
    paddingRight: 10,
  },
  back: {
    flexDirection: 'row',
    width: '100%',
  },
  backButton: {
    alignItems: 'center',
  },
  backTitle: {
    color: 'white',
    textAlignVertical: 'center',
  },
});

export default HeaderBack;
