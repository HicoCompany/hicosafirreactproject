import { Dimensions } from 'react-native';
import { NavigationActions } from 'react-navigation';

export const SpinnerType =
{
    CircleFlip: 'CircleFlip',
    Bounce: 'Bounce',
    Wave: 'Wave',
    WanderingCubes: 'WanderingCubes',
    Pulse: 'Pulse',
    ChasingDots: 'ChasingDots',
    ThreeBounce: 'ThreeBounce',
    Circle: 'Circle',
    CubeGrid: '9CubeGrid',
    WordPress_IOS: 'WordPress',
    FadingCircle: 'FadingCircle',
    FadingCircleAlt: 'FadingCircleAlt',
    Arc_IOS: 'Arc',
    ArcAlt_IOS: 'ArcAlt',
};

export const Navigate = (context, routeName, params) => {
    if (context
        && context.props
        && context.props.navigation) {
        context.props.navigation.navigate({ routeName, params, key: Date.now() });
    }
};

export const setParamsAction = (screen, params) => NavigationActions.setParams({
    params,
    key: screen,
});

export const GetDim = () => {
    return Dimensions.get('window');
};

const Constants =
{

    UPLOAD_STATUS: {
        IDLE: 0,
        UPLOADING: 1,
        DOWNLOADING: 2,
        ERROR: 3,
        DONE: 4,
    },

    PageSize: 10,
    Offset: 0,

     //API_URL: 'http://212.86.75.186:9094/Service.svc/',
API_URL:'https://ecoapp.ahvaz.ir/Service.svc/',
    SUPPORT_CENTER_PHONE_NUMBER: '+98513000313',

    SOUND_HORN: 'horn.mp3',
};

export default Constants;
