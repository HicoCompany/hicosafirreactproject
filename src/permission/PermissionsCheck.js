import Permissions from 'react-native-permissions';

export default class PermissionsCheck {

    static async location () // android and ios
    {
        return Permissions.check('location');
    }
    static async camera () // android and ios
    {
        return Permissions.check('camera');
    }
    static microphone (callback) // android and ios
    {
        Permissions.check('microphone').then(callback);
    }
    static photo (callback) // android and ios
    {
        Permissions.check('photo').then(callback);
    }
    static contacts (callback) // android and ios
    {
        Permissions.check('contacts').then(callback);
    }
    static event (callback) // android and ios
    {
        Permissions.check('event').then(callback);
    }
    static bluetooth (callback) // just ios
    {
        Permissions.check('bluetooth').then(callback);
    }
    static reminder (callback) // just ios
    {
        Permissions.check('reminder').then(callback);
    }
    static notification (callback) // just ios
    {
        Permissions.check('notification').then(callback);
    }
    static backgroundRefresh (callback) // just ios
    {
        Permissions.check('backgroundRefresh').then(callback);
    }
    static speechRecognition (callback) // just ios
    {
        Permissions.check('speechRecognition').then(callback);
    }
    static mediaLibrary (callback) // just ios
    {
        Permissions.check('mediaLibrary').then(callback);
    }
    static motion (callback) // just ios
    {
        Permissions.check('motion').then(callback);
    }
    static storage (callback) // just android
    {
        Permissions.check('storage').then(callback);
    }
    static callPhone (callback) // just android
    {
        Permissions.check('callPhone').then(callback);
    }
    static readSms (callback) // just android
    {
        Permissions.check('readSms').then(callback);
    }
    static receiveSms (callback)  // just android
    {
        Permissions.check('receiveSms').then(callback);
    }

}
