import React, { Component } from 'react';
import {
  FlatList,
  Image,
  StatusBar,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Card, Icon, Container, Content, Picker } from 'native-base';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import { Button, Empty, Loading } from '../../../components';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';

let id = -1;

class ListPhoneGetDetailNewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      listWaste: [],

      refreshing: false,
      laps: [],
      apiError: false,
      apiErrorDesc: '',
    };

    id = this.props.navigation.getParam('DeliveryWasteID');
  }

  componentDidMount = () => {
    this.getApi();
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  componentWillUnmount = () => {};

  getApi = async () => {
    await this.phoneGetDeliveredWasteReportItemContractorApi();
    await this.PhoneGetWastTypeForContractorApi();
  };
  renderAreaPickerItem() {
    return this.state.listWaste.map(Waste => (
      <Picker.Item
        key={Waste.ID.toString()}
        label={`${Waste.Title}`}
        value={Waste.ID}
      />
    ));
  }
  async PhoneGetWastTypeForContractorApi() {
    const headers = null;
    const params = {
      UserName: this.props.userData.UserName,
      Password: this.props.userData.Password,
    };

    try {
      const response = await Repository.PhoneGetWastTypeForContractorApi(
        params,
        headers,
      );

      if (response.ResultID === 100) {
        const listWaste = response.Result.map(item => {
          return {
            ID: item.WasteTypeID,
            Count: 1,
            Point: item.Point,
            PointSum: item.Point,
            Price: item.HelperPrice,
            Title: item.Title,
            TotlaPrice: item.HelperPrice,
          };
        });
        this.setState({
          listWaste: listWaste,
        });
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }
  async phoneGetDeliveredWasteReportItemContractorApi() {
    const headers = null;
    const params = {
      UserName: this.props.userData.UserName,
      Password: this.props.userData.Password,
      DeliveryWasteID: id,
    };
    try {
      const response = await Repository.PhoneGetDeliveredWasteReportItemContractorApi(
        params,
        headers,
      );
      if (response.Success === 1) {
        const listWaste = response.Result.map(item => {
          return {
            ID: item.ReqWasteId,
            Count: item.WasteTypeCount,
            Point: item.Point,
            PointSum:
              (Number.isNaN(parseInt(item.Point)) ? 0 : parseInt(item.Point)) *
              (Number.isNaN(parseInt(item.WasteTypeCount))
                ? 0
                : parseInt(item.WasteTypeCount)),
            Price: item.Price,
            Title: item.WasteTypeStr,
            TotlaPrice: item.TotlaPrice,
          };
        });

        this.setState({
          list: listWaste,
        });
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  lapsList() {
    return this.state.list.map(data => {
      return (
        <View style={[styles.TotalCard]}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Card style={[styles.CardSelect]}>
              <TouchableOpacity
                style={[styles.Btn]}
                onPress={() => {
                  this.setState(
                    {
                      apiError: false,
                      isLoadingPage: true,
                    },
                    () => {
                      this.PhoneGetWastTypeDeleted(data.ID);
                    },
                  );
                }}>
                <Icon
                  type="AntDesign"
                  name="close"
                  style={[styles.IconGreen, { color: 'red' }]}
                />
              </TouchableOpacity>
              <Text style={[styles.text]}>{data.TotlaPrice} تومان</Text>
              <Text style={[styles.text]}>{data.PointSum} امتیاز </Text>
              <TextInput
                placeholder={'وزن'}
                keyboardType={'numeric'}
                value={data.Count.toString()}
                onChangeText={count => {
                  this.PhoneGetWastTypeCount(data.ID, count);
                }}
              />
              <Text style={[styles.text]}>{data.Title}</Text>
            </Card>
          </View>
        </View>
      );
    });
  }
  PhoneGetWastTypeDeleted(WasteTypeID) {
    this.setState({
      list: this.state.list.filter(el => el.ID !== WasteTypeID),
      // list: this.state.list.concat(this.state.laps.filter(el=>el.WasteTypeID==WasteTypeID))
    });
  }
  PhoneGetWastTypePlus() {
    var listtest = this.state.listWaste.filter(
      el => el.ID === this.state.WasteTypeID,
    );
    this.setState({
      list: this.state.list.concat(listtest),
    });
    // alert(JSON.stringify(this.state.list));
    //this.setState({list:list});
  }
  async PhoneAddRequestDeliverWasteForContractor() {
    this.setState({
      ApiSuccsess: false,
      ApiSuccessDesc: '',
    });
    const headers = null;
    var list = this.state.list.map(item => {
      return { WasteTypeID: item.ID, WasteTypeCount: item.Count };
    });
    var listcount = list.filter(x => x.WasteTypeCount <= 0).length;
    if (listcount > 0) {
      alert('وزن باید عددی بزرگتر از صفر باشد');
      return;
    }

    //   this.props.navigation.navigate("DeliveryTimeScreen",{laps:this.state.laps})
    const params = {
      UserName: this.props.userData.UserName,
      Password: this.props.userData.Password,
      DeliveryWasteID: this.props.navigation.getParam('DeliveryWasteID'),
      DeliveredWasteList: list,
    };
    try {
      const response = await Repository.PhoneAddRequestDeliverWasteForContractorApi(
        params,
        headers,
      );
      if (response.Success === 1) {
        this.setState({
          ApiSuccsess: true,
          ApiSuccessDesc: response.Text,
        });
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
      this.setState({
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }
  PhoneGetWastTypeCount(WasteTypeID, count) {
    var list = this.state.list.map(item => {
      if (
        item.ID === this.state.list.filter(el => el.ID == WasteTypeID)[0].ID
      ) {
        var countnumber = Number.isNaN(parseInt(count)) ? 0 : parseInt(count);
        item.Count = countnumber.toString();
        item.PointSum =
          parseInt(countnumber) *
          (Number.isNaN(parseInt(item.Point)) ? 0 : parseInt(item.Point));
        item.TotlaPrice = countnumber * item.Price;
        return item;
      } else {
        return item;
      }
    });
    this.setState({ list: list });
  }
  PhoneGetWastTypeMinus(WasteTypeID) {
    if (
      this.state.laps.filter(el => el.WasteTypeID == WasteTypeID)[0].count > 1
    ) {
      var list = this.state.laps.map(item => {
        if (
          item.WasteTypeID ===
          this.state.laps.filter(el => el.WasteTypeID == WasteTypeID)[0]
            .WasteTypeID
        ) {
          item.count = item.count - 1;

          return item;
        } else {
          return item;
        }
      });

      this.setState({ laps: list });
    }
  }
  handleRefresh = () => {
    this.setState(
      {
        refreshing: true,
        isLoadingPage: true,
        list: [],
      },
      () => {
        this.phoneGetDeliveredWasteReportItemContractorApi();
      },
    );
  };

  renderEmpty = () => {
    return this.state.isLoadingPage ? (
      <View style={{ flex: 1 }}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={{ flex: 1 }}>
        <Empty />
      </View>
    );
  };

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack
          onBackPress={() => this.props.navigation.goBack()}
          headerTitle={'جزئیات جمع آوری پسماند'}
        />
        <Content style={{ flex: 1 }}>
          {this.lapsList()}
          <View style={styles.pickerContainer}>
            <Picker
              style={styles.pickerStyle}
              textStyle={styles.pickerTextStyle}
              iosHeader="کالای جدید مورد نظر را انتخاب کنید"
              headerBackButtonText="بستن"
              iosIcon={<Icon name="ios-arrow-down-outline" />}
              headerStyle={styles.pickerHeaderStyle}
              headerTitleStyle={styles.pickerHeaderTextStyle}
              headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
              itemStyle={styles.pickerItemStyle}
              itemTextStyle={styles.pickerItemTextStyle}
              selectedValue={this.state.WasteTypeID}
              onValueChange={WasteTypeID => {
                this.setState({ WasteTypeID });
              }}
              mode={'dropdown'}>
              {this.renderAreaPickerItem()}
            </Picker>
            <Button
              buttonText={
                this.state.isLoadingButton
                  ? 'لطفا منتظر بمانید'
                  : 'افزودن پسماند جدید'
              }
              isLoading={this.state.isLoadingButton}
              style={styles.button}
              onPress={() => {
                this.PhoneGetWastTypePlus();
              }}
            />
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingTop: 10,
            }}>
            <TouchableOpacity style={[styles.BtnSubmit]}>
              <Text style={[styles.textFooter]}>
                مجموع امتیاز این درخواست:{' '}
                {this.state.list.reduce((a, v) => a + v.PointSum * v.Count, 0)}
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingTop: 10,
            }}>
            <Button
              buttonText={
                this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'ثبت پسماند'
              }
              isLoading={this.state.isLoadingButton}
              style={styles.button}
              onPress={() => {
                this.PhoneAddRequestDeliverWasteForContractor();
              }}
            />
          </View>
          <Portal>
            <Dialog
              visible={this.state.apiError}
              style={styles.dialogContainer}
              dismissable
              onDismiss={() => {
                this.setState({
                  apiError: false,
                });
              }}>
              <Dialog.Content>
                <Text style={styles.dialogText}>{this.state.apiErrorDesc}</Text>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  buttonText={'تلاش مجدد'}
                  onPress={() => {
                    this.setState(
                      {
                        apiError: false,
                        isLoadingPage: true,
                      },
                      () => {
                        this.getApi();
                      },
                    );
                  }}
                  style={styles.dialogButton}
                />
              </Dialog.Actions>
            </Dialog>
            <Dialog
              visible={this.state.ApiSuccsess}
              style={styles.dialogContainer}
              dismissable
              onDismiss={() => {
                this.setState({
                  ApiSuccsess: false,
                });
              }}>
              <Dialog.Content>
                <Text style={styles.dialogText}>
                  {this.state.ApiSuccessDesc}
                </Text>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  buttonText={'تایید و بازگشت'}
                  onPress={() => {
                    this.props.navigation.navigate('WasteCollectionRequests');
                  }}
                  style={styles.dialogButton}
                />
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </Content>
      </Container>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  textFooter: {
    color: Colors.white,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
  },

  BtnSubmit: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#50b3ae',
    borderRadius: 10,
    padding: 7,
    width: '60%',
    color: Colors.white,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
  },
  text: {
    color: 'black',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
  },
  TotalCard: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  IconGreen: {
    color: '#50b3ae',
    fontSize: 20,
  },
  CardSelect: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 5,
    padding: 8,
    paddingVertical: 12,
  },
  itemContainer: {
    flexDirection: 'row',
    margin: 10,
    borderRadius: 15,
    borderColor: Colors.gray,
    borderWidth: 1,
    paddingTop: 5,
    paddingBottom: 5,
  },
  itemRightContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 10,
  },
  itemImage: {
    height: wp('12%'),
    width: wp('12%'),
    marginRight: 5,
  },
  itemLeftContainer: {
    flex: 1,
  },
  itemTextContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  itemTextTitle: {
    fontSize: normalize(10),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
  },
  itemTextDesc: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textGray,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  dialogText: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
});

const mapStateToProps = state => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(ListPhoneGetDetailNewScreen);
