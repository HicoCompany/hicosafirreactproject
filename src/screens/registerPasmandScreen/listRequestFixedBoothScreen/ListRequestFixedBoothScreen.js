import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image, StatusBar, Text, TouchableOpacity, View,TextInput } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import HeaderBack from '../../../components/HeaderBack';
import TabItem from '../../../components/TabItem';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';

class ListRequestFixedBoothScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'لیست جمع آوری پسماند غرفه های ثابت'}
                    source={require('./../../../assets/image/ic_notice.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            refreshing: false,
            loadingData: false,
            HelperName:'',
            PCreateDateFrom:'',
            PCreateDateTo:'',
            MainArea:'0',
            HelperNationalCode:'',
            Mobile:'',
            apiError: false,
            apiErrorDesc: '',
            apiSearch: false,
            apiSearchDesc: ''
        };
    }

    componentDidMount = () => {
        this.getApi();
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    getApi = async () => {
        await this.PhoneSearchHelper();
    }

    async PhoneSearchHelper() {
        const headers = null;
        const params = {
            UserName: this.props.userData.UserName,
            Password: this.props.userData.Password,
            HelperName:this.state.HelperName,
            PCreateDateFrom:this.state.PCreateDateFrom,
            PCreateDateTo:this.state.PCreateDateTo,
            MainArea:this.state.MainArea,
            HelperNationalCode:this.state.HelperNationalCode,
            Mobile:this.state.Mobile,
            page:1
        };
        try {
            const response = await Repository.PhoneSearchHelperApi(params, headers);
            if (response.ResultID === 100) {
                const list = response.Result;
                if (list !== null) {
                    this.setState({
                        list
                    });
                }
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleLoadMore = () => {
        // if (this.state.countList > 0) {
        //     this.setState({
        //         offset: this.state.offset + this.state.pageSize, loadingData: true
        //     }, async () => {
        //         await this.phoneGetNotificationForHelperApi();
        //     });
        // }
    }

    handleRefresh = () => {
        this.setState({ 
            refreshing: true, 
            isLoadingPage: true,
            list: [],
        }, () => {
            this.PhoneGetListRequestPackage();
        });
    }

    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }

    renderFooter = () => {
        if (!this.state.loadingData) {
            return null;
        }
        return (<ActivityIndicator size={'small'} color={'black'} />);
    }

    renderItem(item) {
        const {
            Address,
            HelperId,
            HelperName,
            MainAreTitle,
            Mobile,
            NationalCode,
            Point,
            UserRole
        } = item;
        return (
            <TouchableOpacity
                style={styles.itemContainer}
                activeOpacity={0.7}
                onPress={() => {
                    this.props.navigation.navigate('DeliveredwasteNewScreen', {
                        HelperId:HelperId,
                        Mobile:Mobile
                    });
                }}
            >
                <View style={styles.itemLeftContainer}>
                    <Text style={styles.itemTextTitle}>
                        {HelperName}
                    </Text>
                    <Text style={styles.itemTextTitle}>
                        {`كد ملي: ${NationalCode}`}
                    </Text>
                    <Text style={styles.itemTextTitle}>
                        {`موبايل : ${Mobile}`}
                    </Text>
                    <Text style={styles.itemTextTitle}>
                        {` منطقه : ${MainAreTitle}`}
                    </Text>
                    <Text style={styles.itemTextTitle}>
                        {` آدرس : ${Address}`}
                    </Text>
                    {/* <Text style={styles.itemTextTitle}>
                        {` امتياز هميار : ${Point}`}
                    </Text> */}
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'ليست جمع آوري پسماند غرفه هاي ثابت'}

                />
                <View style={styles.screenHeader}>
                    <Text style={styles.screenHeaderTitle}>
                        {'همیار مورد نظر خود را انتخاب نمایید'}
                    </Text>
                </View>
                <View>
                <Button 
                                buttonText={'جستجوي هميار'}
                                onPress={() => {
                                    this.setState({
                                        apiSearch: true
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                </View>
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    ListFooterComponent={() => this.renderFooter()}
                    numColumns={1}
                    keyExtractor={(item, index) => item.HelperId.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    onEndReached={() => this.handleLoadMore()}
                    onEndReachedThreshold={0.08}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
                <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiError: false,
                            });
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button 
                                buttonText={'تلاش مجدد'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false,
                                        isLoadingPage: true,
                                        list: []
                                    }, () => {
                                        this.getApi();
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                        </Dialog.Actions>
                    </Dialog>
                    <Dialog
                        visible={this.state.apiSearch}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiSearch: false,
                            });
                        }}
                    >
                        <Dialog.Content>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'نام و نام خانوادگی هميار را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.HelperName}
                                onChangeText={(HelperName) => {
                                    this.setState({
                                        HelperName:HelperName
                                    });
                                }}
                                
                            />
                           
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'شماره همراه هميار را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'numeric'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.Mobile}
                                onChangeText={(Mobile) => {
                                    this.setState({
                                        Mobile:Utils.toEnglishDigits(Mobile)
                                    });
                                }}
                                
                            />
                           
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'كد ملي هميار را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'numeric'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.HelperNationalCode}
                                onChangeText={(HelperNationalCode) => {
                                    this.setState({
                                        HelperNationalCode:Utils.toEnglishDigits(HelperNationalCode)
                                    });
                                }}
                            />
                        </View>

                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button 
                                buttonText={'جستجو'}
                                onPress={() => {
                                    this.setState({
                                        apiSearch: false,
                                        isLoadingPage: true,
                                        list: []
                                    }, () => {
                                        this.PhoneSearchHelper();
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    screenHeader: {
        height: hp('7%'),
        backgroundColor: '#424242',
        justifyContent: 'center',
        alignItems: 'center',
    },
    screenHeaderTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#fff',
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },

    itemContainer: {
        flexDirection: 'row',
        height: hp('20%'),
        margin: 10,
        borderRadius: 15,
        borderColor: Colors.gray,
        borderWidth: 1
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('15%'),
        width: wp('15%'),
        marginRight: 10
    },
    itemLeftContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    itemTextTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },
    itemTextDate: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },
    inputContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderRadius: 5,
        borderWidth: 1,
        marginBottom: 10,
        paddingRight: 10,
        paddingLeft: 10,
        overflow: 'hidden'
    },
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    dialogText: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(ListRequestFixedBoothScreen);
