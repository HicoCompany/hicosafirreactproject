import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,AppRegistry, StyleSheet, Linking, } from 'react-native';
import { Text,Card, CardItem,Container, Content, } from 'native-base';
import { Portal, Dialog } from 'react-native-paper';
// import QRCodeScanner from 'react-native-qrcode-scanner';
import Repository from '../../../repository/Repository';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import TabItem from '../../../components/TabItem';
import HeaderMain2 from '../../../components/HeaderMain2';
import { Button } from '../../../components';
import { hp, normalize, wp } from '../../../responsive';

class QrcodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoadingPage: true,
        apiError: false,
        apiSuccess: false,
        apiErrorDesc: '',
        ResultID:0,
        Type:0
    };
}
componentDidMount = () => {
    
  this.setState({
    ResultID: this.props.navigation.getParam('ResultID'),
    Type:this.props.navigation.getParam('Type')
});
}



async PhoneDeliveryRequestPackageApi(Serial) {
    alert(Serial);
    const headers = null;
    const params = {
        UserName: this.props.userData.UserName,
        Password: this.props.userData.Password,
        RequestPackageId:this.state.ResultID,
        Serial: Serial
        };
    try {
        const response = await Repository.PhoneDeliveryRequestPackageApi(params, headers);
        if (response.Success === 1) {
            this.setState({
                apiSuccess: true,
            });
            setTimeout(()=>{ 
              this.setState({
              apiSuccess: false,
              
          });
          this.props.navigation.navigate('profile');
      }, 2000);
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        if (this.mounted) {
            this.setState({
                isLoadingButton: false,
            });
        }
    } catch (error) {
        if (this.mounted) {
            this.setState({
                isLoadingButton: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
  }


async PhonePackageRegistrationApi(Serial) {
  alert(Serial);
  const headers = null;
  const params = {
      UserName: this.props.userData.UserName,
      Password: this.props.userData.Password,
      HelperID:this.state.ResultID,
      Serial: Serial
      };
  try {
      const response = await Repository.PhonePackageRegistrationApi(params, headers);
      if (response.Success === 1) {
          this.setState({
              apiSuccess: true,
          });
          setTimeout(()=>{ 
            this.setState({
            apiSuccess: false,
            
        });
        this.props.navigation.navigate('profile');
    }, 2000);
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      if (this.mounted) {
          this.setState({
              isLoadingButton: false,
          });
      }
  } catch (error) {
      if (this.mounted) {
          this.setState({
              isLoadingButton: false,
              apiError: true,
              apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
          });
      }
  }
}
  async onSuccess(e) {
   if(this.state.Type==1)
   await this.PhonePackageRegistrationApi(String(e.data));
   else if(this.state.Type==2)
   await this.PhoneDeliveryRequestPackageApi(String(e.data));
  }
    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'ارائه پسماند'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };

    render() {
        return (
            <Container>
          <Content>

            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                 <HeaderMain2
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    
                    headerTitle={'ارائه پسماند '}
                />
               
            <View style={{flex:1,flexDirection:'row'}}>
                
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
                {/* <QRCodeScanner
        onRead={this.onSuccess.bind(this)}
     
      /> */}
              </View>
           
           </View>
            <TouchableOpacity
                style={[styles.BtnProduct]}
                onPress={() => this.props.navigation.navigate("ProductDetails")}
              >
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}>{this.state.ResultID} {this.state.Type} </Text>
              </View>
              
              </View>
           
            </TouchableOpacity>
            </View>
            <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        dismissable={false}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignSelf: 'center'
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button
                                buttonText={'تایید'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false
                                    });
                                }}
                                style={{ width: wp('40%') }}
                            />
                        </Dialog.Actions>
                    </Dialog>
                    <Dialog
                        visible={this.state.apiSuccess}
                        dismissable={false}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignSelf: 'center'
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {'ثبت پکیج با موفقیت انجام شد'}
                            </Text>
                        </Dialog.Content>
                       
                    </Dialog>
                </Portal>
            </Content>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    centerText: {
      flex: 1,
      fontSize: 18,
     
      color: '#777',
    },
    textBold: {
      fontWeight: '500',
      color: '#000',
    },
    buttonText: {
      fontSize: 21,
      color: 'rgb(0,122,255)',
    },
    buttonTouchable: {
     
    },
    IRANSansNormal: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     IRANSansBold: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
     },
     header:{
      backgroundColor:'#1a237e',
     },
     badge:{
      position: 'absolute',
      top: 8, 
      right: 10, 
  },
      BtnImg:{
        width:'100%',
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
      },
      textBtn:{
        color:'white',
        fontFamily: 'IRANSans(FaNum)',
        fontSize:14,
      },
      bgProduct:{
        paddingHorizontal:10,
        paddingVertical:10,
        backgroundColor:'#fafafa',
        borderBottomWidth:1,
        borderBottomColor:"#f0f0f0"
      },
      BoxProduct:{
        width:160,
        height:230,
        backgroundColor: 'white',
        alignItems: 'center',
      },
      ImgProduct:{
        width:'85%',
        height: 80,
        resizeMode:'contain',
       
      },
      textProduct: {
        color: 'black',
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
      },
      textGreen:{
        color: '#2e7d32',
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
      },
      textRed:{
        color: '#e53935',
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: "#000"
      },
      BtnProduct:{
          borderWidth:1,
          borderColor:'#f0f0f0',
          height:40,
          justifyContent:"center",
         alignItems:"center"
      },
      BtnProductOne:{
       flex:2,
       justifyContent:"center",
       alignItems:"center",
       
       
       backgroundColor:'green'
      },
      BtnProductTwo:{
        flex:1,backgroundColor:'white',
        justifyContent:"center",
        alignItems:"center",
        borderBottomRightRadius:10,
        borderTopRightRadius:10,
      },
      textBtnProduct:{
       color:'white',
       fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
      BoxBtnSort:{
        flex:1,
        flexDirection:'row',
        justifyContent:"center",
        alignItems:"center",
        borderWidth:0.5, 
        borderColor:"#bdbbbb",
        width:330,
      },
      BtnSort:{
          width:160,
          height:40,
        paddingHorizontal:4,
        backgroundColor:'#fafafa',
        alignItems:"center",
      }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(QrcodeScreen);
