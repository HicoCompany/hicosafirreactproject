import React, { Component } from 'react';
import { StatusBar, Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderMain2 from '../../../components/HeaderMain2';

class ResidueScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'ارائه پسماند'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                 <HeaderMain2
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    
                    headerTitle={'ارائه پسماند'}
                />
                <Text style={styles.title}>
                    {'ارائه پسماند'}
                </Text>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(ResidueScreen);
