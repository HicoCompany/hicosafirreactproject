import { Tab, Tabs, View } from 'native-base';
import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import HeaderMain2 from '../../../components/HeaderMain2';
import TabItem from '../../../components/TabItem';
import { hp, normalize } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';
import WareReport from './component/WareReport';
import FinancialReport from './component/FinancialReport';
import WasteReport from './component/WasteReport';

class ReportScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={' گزارشات'}
                    source={require('./../../../assets/image/ic_report.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
        super(props);
        this.tabs = null;
    }

    componentDidMount = () => {
        setTimeout(this.tabs.goToPage.bind(this.tabs, 2));
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
               <HeaderMain2
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    
                    headerTitle={'گزارشات '}
                />
                <Tabs
                    ref={tabs => (this.tabs = tabs)}
                    tabContainerStyle={styles.tabs}
                    initialPage={1}
                    tabBarPosition={'top'}
                    tabBarUnderlineStyle={styles.tabBarUnderLineColor}
                    locked
                >
                    <Tab
                        heading={'مالی پیمانکار'}
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.tabActiveStyle}
                        textStyle={styles.tabText}
                        activeTextStyle={styles.tabActiveText}
                    >
                        <FinancialReport
                            navigation={this.props.navigation}
                        />
                    </Tab>
                    <Tab
                        heading={'کالاهای تحویلی'}
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.tabActiveStyle}
                        textStyle={styles.tabText}
                        activeTextStyle={styles.tabActiveText}
                    >
                        <WareReport
                            navigation={this.props.navigation}
                        />
                    </Tab>
                    <Tab
                        heading={'پسماند دریافتی'}
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.tabActiveStyle}
                        textStyle={styles.tabText}
                        activeTextStyle={styles.tabActiveText}
                    >
                        <WasteReport
                            navigation={this.props.navigation}
                        />
                    </Tab>
                </Tabs>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    tabs: {
        height: hp('10%'),
        // backgroundColor: 'transparent',
    },
    tabBarUnderLineColor: {
        backgroundColor: 'transparent'
    },
    tabStyle: {
        backgroundColor: Colors.statusBar,
        color:Colors.white

    },
    tabActiveStyle: {
        backgroundColor: Colors.green  
      },
    tabText: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.white,
    },
    tabActiveText: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        textAlign: 'center',
        color: Colors.white,
        fontWeight: 'normal'
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(ReportScreen);
