import React, { Component } from 'react';
import {
  Linking,
  ActivityIndicator,
  FlatList,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';

//ui
import { Dialog, Portal } from 'react-native-paper';
import { Button, Empty, Loading } from '@components';
import HeaderBack from '@components/HeaderBack';

//utils
import Colors from '@src/utility/Colors';
import Utils from '@src/utility/Utils';
import { hp, normalize, wp } from '@src/responsive';
import EStyleSheet from 'react-native-extended-stylesheet';

//network
import Repository from '@src/repository/Repository';

// private component
import Card from './components/card';

class WasteCollectionRequests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      listWaste: [],
      refreshing: false,
      loadingData: false,
      apiError: false,
      apiErrorDesc: '',
    };
  }

  componentDidMount = () => {
    this.getApi();
  };

  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  };

  getApi = async () => {
    await this.PhoneGetDeliverWasteForContractor();
    await this.PhoneGetWastTypeForContractorApi();
  };

  async PhoneGetWastTypeForContractorApi() {
    const headers = null;
    const params = {
      UserName: this.props.userData.UserName,
      Password: this.props.userData.Password,
    };

    try {
      const response = await Repository.PhoneGetWastTypeForContractorApi(
        params,
        headers,
      );

      if (response.ResultID === 100) {
        const listWaste = response.Result;
        this.setState({
          listWaste,
        });
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }
  async PhoneGetDeliverWasteForContractor() {
    const headers = null;
    const params = {
      UserName: this.props.userData.UserName,
      Password: this.props.userData.Password,
      page: 1,
    };
    try {
      const response = await Repository.PhoneGetDeliverWasteForContractorApi(
        params,
        headers,
      );
      if (response.ResultID === 100) {
        const list = response.Result;
        if (list !== null) {
          this.setState({
            list,
          });
        }
      } else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  handleLoadMore = () => {
    // if (this.state.countList > 0) {
    //     this.setState({
    //         offset: this.state.offset + this.state.pageSize, loadingData: true
    //     }, async () => {
    //         await this.phoneGetNotificationForHelperApi();
    //     });
    // }
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true,
        isLoadingPage: true,
        list: [],
      },
      () => {
        this.PhoneGetDeliverWasteForContractor();
      },
    );
  };

  renderEmpty = () => {
    return this.state.isLoadingPage ? (
      <View style={{ flex: 1 }}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
      <View style={{ flex: 1 }}>
        <Empty />
      </View>
    );
  };

  renderFooter = () => {
    if (!this.state.loadingData) {
      return null;
    }
    return <ActivityIndicator size={'small'} color={'black'} />;
  };

  handleAccept = () => {
    this.props.navigation.navigate('ListPhoneGetDetailNew', {
      DeliveryWasteID: DeliveryWasteID,
    });
  };

  renderItem = ({ item }) => {
    const { DeliveryWasteID } = item;
    return <Card {...item} handleAccept={this.handleAccept} />;
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack
          onBackPress={() => this.props.navigation.goBack()}
          headerTitle={'لیست جمع آوری پسماند '}
        />

        <View style={styles.screenHeader}>
          <Text style={styles.screenHeaderTitle}>
            {'همیاری مورد نظر را انتخاب نمایید'}
          </Text>
        </View>
        <FlatList
          data={this.state.list}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty}
          ListFooterComponent={this.renderFooter}
          keyExtractor={(item, index) => item.DeliveryWasteID.toString()}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={0.08}
          contentContainerStyle={
            this.state.list.length > 0 ? styles.list : { flex: 1 }
          }
        />
        <Portal>
          <Dialog
            visible={this.state.apiError}
            style={styles.dialogContainer}
            dismissable
            onDismiss={() => {
              this.setState({
                apiError: false,
              });
            }}>
            <Dialog.Content>
              <Text style={styles.dialogText}>{this.state.apiErrorDesc}</Text>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                buttonText={'تلاش مجدد'}
                onPress={() => {
                  this.setState(
                    {
                      apiError: false,
                      isLoadingPage: true,
                      list: [],
                    },
                    () => {
                      this.getApi();
                    },
                  );
                }}
                style={styles.dialogButton}
              />
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  screenHeader: {
    height: hp('7%'),
    backgroundColor: '#424242',
    justifyContent: 'center',
    alignItems: 'center',
  },
  list: {
    padding: 16,
  },
  screenHeaderTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#fff',
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },

  itemContainer: {
    flexDirection: 'row',
    height: hp('20%'),
    margin: 5,
    borderRadius: 15,
    borderColor: Colors.gray,
    borderWidth: 1,
  },
  itemRightContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 10,
  },
  itemImage: {
    height: wp('15%'),
    width: wp('15%'),
    marginRight: 10,
  },
  itemLeftContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexWrap: 'wrap',
  },
  itemTextTitle: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginRight: 3,
    flexWrap: 'wrap',
  },
  itemTextDate: {
    fontSize: normalize(10),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textGray,
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  dialogText: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%'),
  },
  itemTextContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
    marginRight: 5,
    marginLeft: 5,
    flexWrap: 'wrap',
    flexGrow: 1,
  },
  itemTextTitle: {
    fontSize: normalize(10),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
  },
  itemTextDesc: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textGray,
    marginLeft: 3,
    flexWrap: 'wrap',
  },
  itemTextDesc1: {
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.blue,
    marginLeft: 3,
    flexWrap: 'wrap',
  },
});

const mapStateToProps = state => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(WasteCollectionRequests);
