import React, { useState, useRef, useCallback } from 'react';
import { View, StyleSheet } from 'react-native';

//ui
import { Button, Card, Divider, useTheme } from 'react-native-paper';
import Label from '@components/Label';
import { theme } from '@src/theme';
import Animated, { Easing } from 'react-native-reanimated';
import { useTransition } from 'react-native-redash/lib/module/v1';

export default function RequestCard({
  HelperName,
  HelperAdress,
  CreateDateStr,
  DeliveryDateStr,
  DeliveryTimeStr,
  HelperMobile,
  handleAccept,
}) {
  const { colors } = useTheme();

  const handleStart = useCallback(() => {
    setProgress(true);
  }, []);
  const handleStop = useCallback(() => {
    setProgress(false);
  }, []);

  return (
    <Card style={styles.container}>
      <Card.Content style={styles.content}>
        <Label style={styles.value}>{HelperName}</Label>
        <View style={styles.section}>
          <Label color="placeholder" style={styles.title}>
            {'آدرس : '}
          </Label>
          <Label style={styles.value}>{HelperAdress}</Label>
        </View>
        <View style={styles.section}>
          <Label
            color="placeholder"
            style={styles.value}>{`تاریخ جمع آوری :`}</Label>
          <Label style={styles.title}>
            {`${DeliveryDateStr} - ${CreateDateStr} - ${DeliveryTimeStr}`}
          </Label>
        </View>

        <Divider />
      </Card.Content>
      <Card.Actions style={styles.actions}>
        <Button
          onTouchStart={handleStart}
          onTouchEnd={handleStop}
          style={styles.accept}>
          قبول درخواست
        </Button>
      </Card.Actions>
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  content: {
    // flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  section: {
    flexDirection: 'column',
    flexWrap: 'wrap',
    // alignItems:
  },
  actions: { justifyContent: 'center' },
  accept: {
    width: '100%',
    borderColor: theme.colors.primary,
    borderWidth: 1,
    position: 'relative',
    zIndex: 1,
  },
});
