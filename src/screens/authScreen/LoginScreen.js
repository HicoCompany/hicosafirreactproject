import React, { Component } from 'react';
import {
  Alert,
  Image,
  Keyboard,
  StatusBar,
  Text,
  TextInput,
  View,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import EStyleSheet from 'react-native-extended-stylesheet';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import Repository from '../../repository/Repository';
import Colors from '../../utility/Colors';
import Utils from '../../utility/Utils';
import { Button } from './../../components';
import { hp, normalize, wp } from './../../responsive';
import { setIsLogin, setDataLogin } from './../../redux/actions';
import NetInfo from '@react-native-community/netinfo';

const action = StackActions.reset({
  index: 0,
  key: null,
  actions: [NavigationActions.navigate({ routeName: 'dashboard' })],
});

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      isLoadingButton: false,

      UserName: '',
      Password: '',
      error: '',
    };

    this.mounted = null;
    this.onEnterClick = this.onEnterClick.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    this.unsubscribe = NetInfo.addEventListener(state =>
      this.handleConnectionChange(state.isConnected),
    );
    NetInfo.fetch().then(state => {
      this.setState({ isConnected: state.isConnected });
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return Utils.shallowCompare(this, nextProps, nextState);
  }

  componentWillUnmount() {
    this.mounted = false;
    this.unsubscribe();
  }

  onEnterClick = () => {
    Keyboard.dismiss();
    const { isConnected, UserName, Password } = this.state;
    if (isConnected) {
      if (UserName.length === 0) {
        this.setState({ error: 'نام کاربری وارد نشده است.' });
      } else if (Password.length === 0) {
        this.setState({ error: 'رمز عبور وارد نشده است.' });
      } else {
        this.setState({ isLoadingButton: true });
        this.loginApi({
          UserName: Utils.toEnglishDigits(UserName),
          Password: Utils.toEnglishDigits(Password),
        });
      }
    } else {
      Alert.alert(
        'اتصال دستگاه خود را بررسی کنید.',
        '',
        [
          { text: 'انصراف' },
          { text: 'تلاش مجدد', onPress: this.onEnterClick },
          { text: 'تایید' },
        ],
        { cancelable: true },
      );
    }
  };

  async loginApi({ UserName, Password }) {
    const DeviceID = DeviceInfo.getUniqueID();
    const headers = {
      'Content-Type': 'application/json',
    };
    try {
      const response = await Repository.LoginApi(
        { UserName, Password, DeviceID },
        headers,
      );
      if (response.Success === 1) {
        this.props.setIsLogin();
        this.props.setDataLogin({ UserName, Password });
        this.props.navigation.dispatch(action);
      } else {
        Alert.alert(response.Text, '', [{ text: 'انصراف' }], {
          cancelable: true,
        });
      }
      if (this.mounted) {
        this.setState({ isLoadingButton: false });
      }
    } catch (error) {
      alert(error);
      if (this.mounted) {
        this.setState({
          isLoadingButton: false,
          error: 'ارتباط دستگاه خود را بررسی کنید.',
        });
      }
    }
  }

  handleConnectionChange = isConnected => {
    this.setState({ isConnected });
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <Image
          style={styles.logo}
          source={require('./../../assets/image/ic_logo_login.png')}
          resizeMode={'contain'}
        />
        {/* <Text style={styles.title}>
          {'ورود'}
        </Text> */}
        {/* <Text style={styles.desc}>
          {'در کادر زیر نام کاربری و رمز عبور خود را وارد کنید'}
        </Text> */}
        <View style={styles.separate} />
        <Text style={styles.inputTitle}>{'نام کاربری'}</Text>
        <TextInput
          placeholder={'نام کاربری خود را وارد کنید'}
          placeholderTextColor={Colors.textGray}
          selectionColor={Colors.textBlack}
          keyboardType={'default'}
          style={styles.input}
          onChangeText={UserName => this.setState({ UserName, error: '' })}
          value={this.state.UserName}
        />
        <Text style={styles.inputTitle}>{'رمز عبور'}</Text>
        <TextInput
          placeholder={'رمز عبور خود را وارد کنید'}
          placeholderTextColor={Colors.textGray}
          selectionColor={Colors.textBlack}
          keyboardType={'default'}
          autoFocus={false}
          style={styles.input}
          onChangeText={Password => this.setState({ Password, error: '' })}
          value={this.state.Password}
          secureTextEntry
        />
        <Text style={styles.error}>{this.state.error}</Text>
        <Button
          style={styles.button}
          buttonText={
            this.state.isLoadingButton
              ? 'لطفا منتظر بمانید'
              : 'ورود به سامانه دوستدار طبیعت'
          }
          isLoadingButton={this.state.isLoadingButton}
          onPress={this.onEnterClick}
        />
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  '@media (min-width: 600) and (min-height: 900)': {
    input: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
      height: hp('5%'),
      width: wp('70%'),
      borderColor: '#035e6f',
      borderWidth: 1,
      borderRadius: 30,
      alignSelf: 'center',
      textAlign: 'center',
    },
    button: {
      height: hp('5%'),
      width: wp('70%'),
    },
  },
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor,
  },
  logo: {
    height: wp('25%'),
    width: wp('25%'),
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10,
  },
  desc: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.textBlack,
    textAlign: 'center',
  },
  separate: {
    marginBottom: 20,
    marginTop: 20,
    height: 0.8,
    width: wp('70%'),
    backgroundColor: Colors.textGray,
    alignSelf: 'center',
  },
  inputTitle: {
    width: wp('70%'),
    alignSelf: 'center',
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.textBlack,
    textAlign: 'right',
    marginBottom: 10,
  },
  input: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    height: hp('6%'),
    width: wp('70%'),
    borderColor: '#035e6f',
    borderWidth: 1,
    borderRadius: 20,
    alignSelf: 'center',
    textAlign: 'center',
  },
  error: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: 'normal',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.error,
    margin: 2,
    fontSize: normalize(14),
    textAlign: 'center',
  },
  button: {
    width: wp('70%'),
  },
});

const mapStateToProps = state => {
  return {
    userData: state.userData,
  };
};

const mapActionToProps = {
  setIsLogin,
  setDataLogin,
};

export default connect(mapStateToProps, mapActionToProps)(LoginScreen);
