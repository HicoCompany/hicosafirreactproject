import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,TextInput, ActivityIndicator, FlatList} from 'react-native';
import { Text,Card, CardItem,Container, Content,Footer, FooterTab,Icon } from 'native-base';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import Colors from '../../../utility/Colors';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';

import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';


class ProductScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={' کالا  '}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          refreshing: false,
          loadingData: false,
          ApiSuccessDesc:'',
          ApiSuccsess:false,
          apiError: false,
          apiErrorDesc: ''
      };
  }

  componentDidMount = () => {
      this.getApi();
  }

  shouldComponentUpdate = (nextProps, nextState) => {
      return Utils.shallowCompare(this, nextProps, nextState);
  }

  componentWillUnmount = () => {

  }

  getApi = async () => {
      await this.PhoneGetWareApi();
  }

  async PhoneGetWareApi() {
      const headers = null;
      const params = {
          Mobile: this.props.userData.mobileNumber,
          Code: this.props.userData.codeNumber
      };
      try {
          const response = await Repository.PhoneGetWareApi(params, headers);
          if (response.ResultID === 100) {
              const list = response.Result;
              this.setState({
                  list
              });
          }
          else {
              this.setState({
                  apiError: true,
                  apiErrorDesc: response.Text
              });
          }
          this.setState({
              loadingData: false,
              refreshing: false,
              isLoadingPage: false,
          });
      } catch (error) {
          await this.setState({
              loadingData: false,
              refreshing: false,
              isLoadingPage: false,
              apiError: true,
              apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
          });
      }
  }

  handleLoadMore = () => {
      // if (this.state.countList > 0) {
      //     this.setState({
      //         offset: this.state.offset + this.state.pageSize, loadingData: true
      //     }, async () => {
      //         await this.phoneGetNotificationForHelperApi();
      //     });
      // }
  }

  handleRefresh = () => {
      this.setState({ 
          refreshing: true, 
          isLoadingPage: true,
          list: [],
      }, () => {
          this.PhoneGetWareApi();
      });
  }

  renderEmpty = () => {
      return this.state.isLoadingPage ? (
          <View style={{ flex: 1 }}>
              <Loading
                  message={'در حال دریافت اطلاعات'}
                  messageColor={Colors.green}
              />
          </View>
      ) : (
              <View style={{ flex: 1 }}>
                  <Empty />
              </View>
          );
  }

  renderFooter = () => {
      if (!this.state.loadingData) {
          return null;
      }
      return (<ActivityIndicator size={'small'} color={'black'} />);
  }
  async PhoneAddRequestWareApi(WareId) {
    this.setState({
        ApiSuccsess: false,
        ApiSuccessDesc: ''
    });
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        WareId:WareId
    };
    try {
        const response = await Repository.PhoneAddRequestWareApi(params, headers);
        if (response.Success === 1) {
            this.setState({
                ApiSuccsess: true,
                ApiSuccessDesc: response.Text
            });
            
            
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            isLoadingPage: false,
        });
    } catch (error) {
        this.setState({
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
  renderItem(item) {
      const {
        ImagePath,
          Title,
          Price,
          WareId

      } = item;
      return (
        <TouchableOpacity
        style={[styles.Btn]}
        onPress={() => {
            this.setState({
                apiError: false,
                isLoadingPage: true,
            }, 
            async  () => {
                await this.PhoneAddRequestWareApi(WareId);
                    });
        }}
      >
      <Card style={styles.BoxProduct}>
          <CardItem header style={{borderBottomWidth:0.5,borderBottomColor:'#50b3ae'}}>
             <Image source={{ uri: ImagePath }} style={styles.ImgProduct}/>
             </CardItem>
      <CardItem  style={{flex:1,flexDirection:'column'}}>
      <View style={{alignItems: 'center',}}>
      <Text style={styles.textProduct}>{Title}</Text>
     
      <Text style={styles.textGreen}>{Price} تومان</Text>
      </View>
      </CardItem>
    </Card>
    </TouchableOpacity>
      );
  }

  render() {
      return (
        <Container>
               
        <Content>
        <StatusBar
                 backgroundColor={Colors.statusBar}
                 barStyle="light-content"
             />
             <HeaderBack
                 onBackPress={() => this.props.navigation.goBack()}
                 headerTitle={'  کالا '}
             />
              <View style={{flex:1,flexDirection:'column',padding:10}}>
              <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
              <FlatList
                  data={this.state.list}
                  renderItem={({ item }) => this.renderItem(item)}
                  ListEmptyComponent={() => this.renderEmpty()}
                  ListFooterComponent={() => this.renderFooter()}
                  numColumns={2}
                  keyExtractor={(item, index) => item.WareId.toString()}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.handleRefresh()}
                  onEndReached={() => this.handleLoadMore()}
                  onEndReachedThreshold={0.08}
                  contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
              />
            </View>
              <Portal>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                              apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      <Dialog.Actions>
                          <Button 
                              buttonText={'تلاش مجدد'}
                              onPress={() => {
                                  this.setState({
                                      apiError: false,
                                      isLoadingPage: true,
                                      list: []
                                  }, () => {
                                      this.getApi();
                                  });
                              }}
                              style={styles.dialogButton}
                          />
                      </Dialog.Actions>
                  </Dialog>
                  <Dialog
                      visible={this.state.ApiSuccsess}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiSuccsess: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiSuccessDesc}
                          </Text>
                      </Dialog.Content>
                     
                  </Dialog>
              </Portal>
                
              </View>
          </Content> 
      </Container>
      );
  }
    

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
     textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
      fontSize: 14,
     },
      text:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 12,
       },
       BoxProduct:{
        width:160,
       // height:230,
        backgroundColor: 'white',
        alignItems: 'center',
        borderRadius: 5,
      },
      ImgProduct:{
        width:'85%',
        height: 80,
        resizeMode:'contain',
       
      },
      textGreen:{
        color: '#50b3ae',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
        fontSize: 12,
      },
      textRed:{
        color: '#e53935',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: "#000"
      },
      dialogButton: {
        width: wp('30%')
    },
      textProduct: {
        color: 'black',
        fontSize: 10,
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
      },
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(ProductScreen);
