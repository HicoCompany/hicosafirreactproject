import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar } from 'react-native';
import { Text,Card, CardItem,Container, Content, } from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { hp, normalize, wp } from '../../../responsive';

import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';

class DeliveryTypeScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'مدیریت اعتبار'}
                    source={require('./../../../assets/image/ic_waste.png')}
                    color={tintColor}
                />,
        };
    };
    componentDidMount = async () => {
      var Mobile = this.props.navigation.getParam('Mobile',0);
     await this.setState({Mobile:Mobile});
      this.getApi();
  }
    render() {
        return (
            <Container>
          <Content>

            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
              {/* <HeaderMain2
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    headerTitle={' مدیریت اعتبار'}
                />
                 */}
                   <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'مدیریت اعتبار'}

                />
               
            <View style={{flex:1,flexDirection:'row',paddingTop:5}}>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
                  <TouchableOpacity
                      style={[styles.BtnDetails]}
                      onPress={() => this.props.navigation.navigate("CashWithdrawalScreen",{Mobile:this.state.Mobile})}
                    >
                    <View>
                      <Card style={styles.BoxProduct}>
                          <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                            <Image source={require('./../../../assets/image/Credit.png')} style={styles.ImgProduct}/>
                            </CardItem>
                            <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}>  برداشت نقدی </Text>
              </View>
            </View>
                      </Card>
                    </View>
                  </TouchableOpacity>
                     
                  <View>
                          
              </View>
            </View>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
                  <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("CharityScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                     <Image source={require('./../../../assets/image/Kheyrieh.png')} style={styles.ImgProduct}/>
                     </CardItem>
             <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}>  خیریه </Text>
              </View>
            </View>
            </Card>
              </View>
            </TouchableOpacity>
                </View>
            </View>

            <View style={{flex:1,flexDirection:'row',paddingTop:20}}>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
            <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("SpecialServiceScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                     <Image source={require('./../../../assets/image/Special.png')} style={styles.ImgProduct}/>
                     </CardItem>
                     <View style={{flex:1,flexDirection:'row',height:40}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}>  کالای خاص </Text>
              </View>
            </View>
            </Card>
              </View>
            </TouchableOpacity>
            </View>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
                  <TouchableOpacity
                      style={[styles.BtnDetails]}
                      onPress={() => this.props.navigation.navigate("ProductScreen")}
                    >
                    <View>
                      <Card style={styles.BoxProduct}>
                          <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                            <Image source={require('./../../../assets/image/products.png')} style={styles.ImgProduct}/>
                            </CardItem>
                            <View style={{flex:1,flexDirection:'row',height:40}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}>  کالا </Text>
              </View>
            </View>
                      </Card>
                    </View>
                  </TouchableOpacity>
                     
            </View>
            </View>
            
            </View>
            </Content>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     IRANSansBold: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
     },
     header:{
      backgroundColor:'#1a237e',
     },
     badge:{
      position: 'absolute',
      top: 8, 
      right: 10, 
  },
      BtnImg:{
        width:'100%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
      },
      textBtn:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        fontSize:14,
      },
      bgProduct:{
        paddingHorizontal:10,
        paddingVertical:10,
        backgroundColor:'#fafafa',
        borderBottomWidth:1,
        borderBottomColor:"#f0f0f0"
      },
      BoxProduct:{
        width:160,
        height:150,
        backgroundColor: 'white',
        alignItems: 'center',
      },
      ImgProduct:{
        width:'85%',
        height: 80,
        resizeMode:'contain',
       
      },
      textProduct: {
        color: 'black',
        fontSize: 11,
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
      },
      textGreen:{
        color: '#2e7d32',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
      },
      textRed:{
        color: '#e53935',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: "#000"
      },
      BtnProduct:{
          borderWidth:1,
          borderColor:'#f0f0f0',
          height:40,
          justifyContent:"center",
         alignItems:"center"
      },
      BtnProductOne:{
       flex:2,
       justifyContent:"center",
       alignItems:"center",
       backgroundColor:'green',
       height:40
      },
      BtnProductTwo:{
        flex:1,backgroundColor:'white',
        justifyContent:"center",
        alignItems:"center",
        borderBottomRightRadius:10,
        borderTopRightRadius:10,
      },
      textBtnProduct:{
       color:'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
       fontSize: 11,
      },
      BoxBtnSort:{
        flex:1,
        flexDirection:'row',
        justifyContent:"center",
        alignItems:"center",
        borderWidth:0.5, 
        borderColor:"#bdbbbb",
        width:330,
      },
      BtnSort:{
          width:160,
          height:40,
        paddingHorizontal:4,
        backgroundColor:'#fafafa',
        alignItems:"center",
      }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(DeliveryTypeScreen);
