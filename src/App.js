import 'react-native-gesture-handler';

import { StyleProvider } from 'native-base';
import React, { Component } from 'react';
import { Provider as PaperProvider } from 'react-native-paper';
import { createAppContainer } from 'react-navigation';
import { connect, Provider } from 'react-redux';
import getTheme from '../native-base-theme/components';
import material from '../native-base-theme/variables/material';
import store from './redux/store';
import { Router } from './Router';
import LocationSender from './backgroundTask/locationSender';
import { theme } from './theme';

const AppContainer = createAppContainer(Router);

class App extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Provider store={store}>
          <LocationSender />
          <PaperProvider theme={theme}>
            <AppContainer />
          </PaperProvider>
        </Provider>
      </StyleProvider>
    );
  }
}

export default App;
