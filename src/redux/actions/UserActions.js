import {
    SET_USER,
    SET_IS_LOGIN,
    SET_IS_SEEN_INTRODUCE,
    SET_DATA_LOGIN,
    EXIT_APP,
    CHANGE_PASSWORD,
    ACTIVE_LOCATION
} from './types';

export const setUser = (data) => {
    return {
        type: SET_USER,
        payload: data
    };
};

export const setIsLogin = () => {
    return {
        type: SET_IS_LOGIN
    };
};

export const setIsSeenIntroduce = () => {
    return {
        type: SET_IS_SEEN_INTRODUCE
    };
};

export const setDataLogin = ({ UserName, Password }) => {
    return {
        type: SET_DATA_LOGIN,
        payload: { UserName, Password }
    };
};

export const changePassword = ({ Password }) => {
    return {
        type: CHANGE_PASSWORD,
        payload: { Password }
    };
};

export const exitApp = () => {
    return {
        type: EXIT_APP,
    };
};

export const activeLocation = (status) => {
    return {
        type: ACTIVE_LOCATION,
        payload: status
    };
};

