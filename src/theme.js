import { configureFonts, DefaultTheme } from 'react-native-paper';

const customFonts = {
  android: {
    light: {
      fontFamily: 'IRANYekanLightMobile(FaNum)',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'IRANYekanMobileBold(FaNum)',
      fontWeight: 'normal',
    },
    regular: {
      fontFamily: 'IRANYekanRegularMobile(FaNum)',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'IRANYekanLightMobile(FaNum)',
      fontWeight: 'normal',
    },
  },
  ios: {
    light: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: 'lighter',
    },
    medium: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: '400',
    },
    regular: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'IRANYekanMobile',
      fontWeight: '100',
    },
  },
};

/**
 * @type {import('react-native-paper/lib/typescript/types').Theme}
 */
export const theme = {
  ...DefaultTheme,
  // Specify custom property
  myOwnProperty: true,
  // Specify custom property in nested object
  colors: {
    ...DefaultTheme.colors,
    primary: '#4caf50',
    background: '#ffffff',
    disabled: '#dadee0',
    text: '#333333',
  },
  fonts: configureFonts(customFonts),
};
